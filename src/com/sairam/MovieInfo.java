package com.sairam;

public class MovieInfo {

    private String title;
    private String studio;
    private String rating;

    public MovieInfo(String title, String studio, String rating) {
        this.title = title;
        this.studio = studio;
        this.rating = rating;
    }

    public MovieInfo(String title, String studio) {
        this.title = title;
        this.studio = studio;
        this.rating = "PG";
    }

    @Override
    public String toString() {
        return "MovieInfo{" +
                "title='" + title + '\'' +
                ", studio='" + studio + '\'' +
                ", rating='" + rating + '\'' +
                '}';
    }

    public static MovieInfo[] getPG(MovieInfo[] movieInfo){
        MovieInfo[] pgMovie=new MovieInfo[movieInfo.length];
        int newArrayIndex=0;
        for (int i = 0; i < movieInfo.length; i++) {
            if(movieInfo[i].rating.equals("PG")){
                pgMovie[newArrayIndex]=movieInfo[i];
                newArrayIndex++;
            }

        }
        return pgMovie;
    }
}
